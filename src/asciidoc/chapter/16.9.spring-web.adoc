[[mvc-config]]
=== Configuring Spring MVC
<<mvc-servlet-special-bean-types>> and <<mvc-servlet-config>> explained about Spring
MVC's special beans and the default implementations used by the `DispatcherServlet`. In
this section you'll learn about two additional ways of configuring Spring MVC. Namely
the MVC Java config and the MVC XML namespace.

The MVC Java config and the MVC namespace provide similar default configuration that
overrides the `DispatcherServlet` defaults. The goal is to spare most applications from
having to having to create the same configuration and also to provide higher-level
constructs for configuring Spring MVC that serve as a simple starting point and require
little or no prior knowledge of the underlying configuration.

You can choose either the MVC Java config or the MVC namespace depending on your
preference. Also as you will see further below, with the MVC Java config it is easier to
see the underlying configuration as well as to make fine-grained customizations directly
to the created Spring MVC beans. But let's start from the beginning.



[[mvc-config-enable]]
==== Enabling the MVC Java Config or the MVC XML Namespace
To enable MVC Java config add the annotation `@EnableWebMvc` to one of your
`@Configuration` classes:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Configuration
	@EnableWebMvc
	public class WebConfig {

	}
----

To achieve the same in XML use the `mvc:annotation-driven` element in your
DispatcherServlet context (or in your root context if you have no DispatcherServlet
context defined):

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<?xml version="1.0" encoding="UTF-8"?>
	<beans xmlns="http://www.springframework.org/schema/beans"
		xmlns:mvc="http://www.springframework.org/schema/mvc"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="
			http://www.springframework.org/schema/beans
			http://www.springframework.org/schema/beans/spring-beans.xsd
			http://www.springframework.org/schema/mvc
			http://www.springframework.org/schema/mvc/spring-mvc.xsd">

		<mvc:annotation-driven />

	</beans>
----

The above registers a `RequestMappingHandlerMapping`, a `RequestMappingHandlerAdapter`,
and an `ExceptionHandlerExceptionResolver` (among others) in support of processing
requests with annotated controller methods using annotations such as `@RequestMapping`,
`@ExceptionHandler`, and others.

It also enables the following:

. Spring 3 style type conversion through a <<core-convert,ConversionService>> instance
in addition to the JavaBeans PropertyEditors used for Data Binding.
. Support for <<format,formatting>> Number fields using the `@NumberFormat` annotation
through the `ConversionService`.
. Support for <<format,formatting>> Date, Calendar, Long, and Joda Time fields using the
`@DateTimeFormat` annotation.
. Support for <<validation-mvc-jsr303,validating>> `@Controller` inputs with `@Valid`, if
a JSR-303 Provider is present on the classpath.
. HttpMessageConverter support for `@RequestBody` method parameters and `@ResponseBody`
method return values from `@RequestMapping` or `@ExceptionHandler` methods.

+

This is the complete list of HttpMessageConverters set up by mvc:annotation-driven:

+

.. `ByteArrayHttpMessageConverter` converts byte arrays.
.. `StringHttpMessageConverter` converts strings.
.. `ResourceHttpMessageConverter` converts to/from
`org.springframework.core.io.Resource` for all media types.
.. `SourceHttpMessageConverter` converts to/from a `javax.xml.transform.Source`.
.. `FormHttpMessageConverter` converts form data to/from a `MultiValueMap<String,
String>`.
.. `Jaxb2RootElementHttpMessageConverter` converts Java objects to/from XML -- added if
JAXB2 is present and Jackson 2 XML extension is not present on the classpath.
.. `MappingJackson2HttpMessageConverter` converts to/from JSON -- added if Jackson 2
is present on the classpath.
.. `MappingJackson2XmlHttpMessageConverter` converts to/from XML -- added if
https://github.com/FasterXML/jackson-dataformat-xml[Jackson 2 XML extension] is present
on the classpath.
.. `AtomFeedHttpMessageConverter` converts Atom feeds -- added if Rome is present on the
classpath.
.. `RssChannelHttpMessageConverter` converts RSS feeds -- added if Rome is present on
the classpath.



