[[mvc-config-view-controller]]
==== View Controllers
This is a shortcut for defining a `ParameterizableViewController` that immediately
forwards to a view when invoked. Use it in static cases when there is no Java controller
logic to execute before the view generates the response.

An example of forwarding a request for `"/"` to a view called `"home"` in Java:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Configuration
	@EnableWebMvc
	public class WebConfig extends WebMvcConfigurerAdapter {

		@Override
		public void addViewControllers(ViewControllerRegistry registry) {
			registry.addViewController("/").setViewName("home");
		}

	}
----

And the same in XML use the `<mvc:view-controller>` element:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<mvc:view-controller path="/" view-name="home"/>
----


[[mvc-config-view-resolvers]]
==== View Resolvers
The MVC config simplifies the registration of view resolvers.

The following is a Java config example that configures content negotiation view
resolution using FreeMarker HTML templates and Jackson as a default `View` for
JSON rendering:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Configuration
	@EnableWebMvc
	public class WebConfig extends WebMvcConfigurerAdapter {

		@Override
		public void configureViewResolvers(ViewResolverRegistry registry) {
			registry.enableContentNegotiation(new MappingJackson2JsonView());
			registry.jsp();
		}

	}
----

And the same in XML:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----
	<mvc:view-resolvers>
		<mvc:content-negotiation>
			<mvc:default-views>
				<bean class="org.springframework.web.servlet.view.json.MappingJackson2JsonView" />
			</mvc:default-views>
		</mvc:content-negotiation>
		<mvc:jsp />
	</mvc:view-resolvers>
----

Note however that FreeMarker, Velocity, Tiles, and Groovy Markup also require
configuration of the underlying view technology.

The MVC namespace provides dedicated elements. For example with FreeMarker:

[source,xml,indent=0]
[subs="verbatim,quotes"]
----

	<mvc:view-resolvers>
		<mvc:content-negotiation>
			<mvc:default-views>
				<bean class="org.springframework.web.servlet.view.json.MappingJackson2JsonView" />
			</mvc:default-views>
		</mvc:content-negotiation>
		<mvc:freemarker cache="false" />
	</mvc:view-resolvers>

	<mvc:freemarker-configurer>
		<mvc:template-loader-path location="/freemarker" />
	</mvc:freemarker-configurer>

----

In Java config simply add the respective "Configurer" bean:

[source,java,indent=0]
[subs="verbatim,quotes"]
----
	@Configuration
	@EnableWebMvc
	public class WebConfig extends WebMvcConfigurerAdapter {

		@Override
		public void configureViewResolvers(ViewResolverRegistry registry) {
			registry.enableContentNegotiation(new MappingJackson2JsonView());
			registry.freeMarker().cache(false);
		}

		@Bean
		public FreeMarkerConfigurer freeMarkerConfigurer() {
			FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
			configurer.setTemplateLoaderPath("/WEB-INF/");
			return configurer;
		}

	}
----



